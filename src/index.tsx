import * as React from "react";
import * as ReactDOM from "react-dom";
import MyComponent from "./myComponent";
import "./scss/reset.scss";

const reactRootElement = document.createElement("div");
document.body.append(reactRootElement);
ReactDOM.render(<MyComponent />, reactRootElement);
