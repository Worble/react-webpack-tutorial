import * as React from "react";
import { hot } from "react-hot-loader";

interface IMyComponentState {
  name: string;
}

class MyComponent extends React.Component<{}, IMyComponentState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      name: ""
    };
  }
  render() {
    return (
      <div>
        <div>
          <label>
            Name:
            <input
              onChange={event => this.setState({ name: event.target.value })}
            ></input>
          </label>
        </div>
        <div>Hello {this.state.name}!</div>
        <img src="my_image.jpg"></img>
      </div>
    );
  }
}

export default hot(module)(MyComponent);
